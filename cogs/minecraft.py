from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.utils import guild_check

guild_id = 0


def is_guild(ctx: commands.Context) -> bool:
    return guild_check(ctx, guild_id, no_name=True)


class Minecraft(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        minecraft_data = self.bot.bot_config['minecraft']
        self.server = minecraft_data['server_id']
        self.key = minecraft_data['api_key']
        self.url = f"https://gamocosm.com/servers/{self.server}/api/{self.key}/"

        global guild_id
        guild_id = int(minecraft_data['server'])
        # TODO: add cog-wide check instead of manually doing is_guild for each
        # TODO: run commands remotely

    async def basic_execute(self, command: str):
        url = self.url + command
        async with self.bot.session.post(url) as r:
            if r.status != 200:
                return r.status
            jsondata = await r.json()
        return jsondata['error']

    async def command_wrapper(self, ctx: commands.Context, command: str):
        async with ctx.typing():
            error = await self.basic_execute(command)
            success = error is None
            out = self.bot.i18n.localize(f"mc_{command}_{success}", ctx=ctx)
            if not success:
                out += str(error)
            await ctx.send(out)

    @commands.group(name="mc", aliases=["minecraft", "pondsmprp"], invoke_without_command=True)
    @commands.check(is_guild)
    async def mc_cmd(self, ctx: commands.Context):
        await ctx.send_help(ctx.command)

    @mc_cmd.command()
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.guild)
    @commands.check(is_guild)
    async def start(self, ctx: commands.Context):
        """
        Starts the physical server.
        """
        await self.command_wrapper(ctx, 'start')

    @mc_cmd.command()
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.guild)
    @commands.check(is_guild)
    @commands.has_permissions(manage_guild=True)
    async def stop(self, ctx: commands.Context):
        """
        Stops the physical server.
        """
        await self.command_wrapper(ctx, 'stop')

    @mc_cmd.command()
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.guild)
    @commands.check(is_guild)
    @commands.has_permissions(manage_guild=True)
    async def resume(self, ctx: commands.Context):
        """
        Starts the Minecraft server.
        """
        await self.command_wrapper(ctx, 'resume')

    @mc_cmd.command()
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.guild)
    @commands.check(is_guild)
    @commands.has_permissions(manage_guild=True)
    async def pause(self, ctx: commands.Context):
        """
        Stops the Minecraft server.
        """
        await self.command_wrapper(ctx, 'pause')

    @mc_cmd.command()
    @commands.cooldown(rate=1, per=30, type=commands.BucketType.guild)
    @commands.check(is_guild)
    @commands.has_permissions(manage_guild=True)
    async def reboot(self, ctx: commands.Context):
        """
        Reboots the physical server.
        """
        await self.command_wrapper(ctx, 'reboot')


def setup(bot: Semicolon):
    if 'minecraft' in bot.bot_config:
        bot.add_cog(Minecraft(bot))
