# semicolon <a href="https://wl.bs2k.ml/engage/semicolon/"><img src="https://wl.bs2k.ml/widgets/semicolon/-/svg-badge.svg" alt="Translation status" /></a>

semicolon is a Discord bot that focuses on simple moderation and user tools.

## Localization

semicolon fully supports localization, from running commands (in supported languages) to giving output!
To get started on translating the bot, head over to [our Weblate](https://wl.bs2k.ml/engage/semicolon/) and sign up with GitHub or GitLab.

## Contributors

Several users have helped semicolon become what it is today, from code contributions to translating the bot.

### Coders

- ;poll was created by [Dunkyl](https://twitter.com/dunkyl_/) and overhauled by [Endr](https://gitlab.com/endrdragon)
- [Valadaptive](https://twitter.com/valadaptiv3/) created the starboard module (_inspired_ by [HTBote](https://gitlab.com/HTSTEM/DiscordBot)) and ported pronouns to a new database.
- [adamthedog](https://gitlab.com/adamthedog1) has made some helpful pull requests and code analysis for bugs

### Translators

These are users who have contributed translations to our [Weblate](https://wl.bs2k.ml/engage/semicolon/) run by [DangerousGlasses](https://gitlab.com/bs2kbs2k).

- Arabic: littlepage
- Esperanto: [Reese Rivers](https://twitter.com/reesetherivers)
- French: [Dunkyl](https://twitter.com/dunkyl_/)
- Korean: [DangerousGlasses](https://gitlab.com/bs2kbs2k)
- Russian: Nekiwo, AlmazikMinecraft
- Spanish: EGAPlumber, [adamthedog](https://gitlab.com/adamthedog1)
- Toki Pona: [anonezumi](https://twitter.com/anonnezumi)
- Pirate: gltile, Adrthegamedev
