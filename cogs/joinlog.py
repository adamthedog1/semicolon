import asyncio
import datetime
import typing

import discord
from discord.ext import commands
from discord.utils import escape_markdown

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.converters import Nullable, SnowflakeConvert, TEXT_CHANNEL, List, USER
from cogs.utils.utils import line_split, shorten, ping_user, get_constant_emoji

joinlog_queue: typing.Dict[int, typing.List[str]] = {}
joinlock = asyncio.Lock()


class Deletable:
    # own class for message delete events bc raw bulk delete doesn't work how i expected and im too lazy to recode
    __slots__ = {'channel_id', 'message_id'}

    def __init__(self, channel_id, message_id):
        self.channel_id = channel_id
        self.message_id = message_id


class Joinlog(commands.Cog):
    channels_config = {
        "joins": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False)),
        "messages": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False)),
        "name_change": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False)),
        "modmail": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False)),
        # "modlog": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False)),
        "roles": Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False))
    }
    misc_config = {
        "ignored_messages_log_users": List(SnowflakeConvert(USER, allow_partial=True))
    }
    message_footer = "\N{BUST IN SILHOUETTE}{0} \N{SPEECH BALLOON}{1} \N{TELEVISION}{2}"

    def __init__(self, bot: Semicolon):
        self.bot = bot
        for k, v in self.channels_config.items():
            self.bot.config_options[k+"_channel"] = v
        for k, v in self.misc_config.items():
            self.bot.config_options[k] = v

    async def joinlog_post(self, channel: discord.TextChannel, message: str, usr, event: str):
        """
        Groups together joinlog messages sent within a second of each other
        :param channel: the channel to post to
        :param message: the message to send
        :param usr: user object or ID to ping (for more consistent client-side ping resolving)
        :param event: event that fired this function
        """
        to_post = False  # whether or not this call of the function will be responsible for posting the final output
        cid = channel.id  # shorthand for ID of the channel

        async with joinlock:  # avoids race conditions with a lock. might want to remove for scalability later
            if cid not in joinlog_queue:
                # first post this second, will take responsibility for posting
                joinlog_queue[cid] = [message]
                to_post = True
            else:
                joinlog_queue[cid].append(message)
        if to_post:
            member = None
            skip_ping = False
            if isinstance(usr, discord.Member):
                member = usr
            elif hasattr(usr, 'id'):
                member = channel.guild.get_member(usr.id)
            elif isinstance(usr, int):
                member = channel.guild.get_member(usr)

            if member and channel.permissions_for(member).read_messages:
                skip_ping = True

            mentions = None if skip_ping else ping_user(usr)

            await asyncio.sleep(1)

            # log prunes
            guild: discord.Guild = channel.guild
            if event == 'LEAVE' and guild.me.guild_permissions.view_audit_log:
                min_dt = datetime.datetime.utcnow() - datetime.timedelta(seconds=3)
                entry_kwargs = {"limit": 50, "oldest_first": False, "action": discord.AuditLogAction.member_prune}
                try:
                    entry = await guild.audit_logs(**entry_kwargs).filter(lambda x: x.created_at >= min_dt).next()
                    extr = entry.extra
                    args = (get_constant_emoji('PRUNE'), entry, extr.members_removed, extr.delete_member_days)
                    joinlog_queue[cid].insert(0, self.bot.i18n.localize("prune_log", guild=guild).format(*args))
                except (discord.NoMoreItems, discord.DiscordServerError):
                    pass

            async with joinlock:
                # split into <=2000 character messages
                for msg in line_split('\n'.join(joinlog_queue[cid])):
                    await channel.send(msg, allowed_mentions=mentions)
                del joinlog_queue[cid]
    
    async def joinlog_process(self, member: discord.Member, guild: discord.Guild, event: str):
        """
        Processes member joins and leaves
        :param member: discord.py member object
        :param guild: the originating guild
        :param event: the event that triggered this function. can be "JOIN", "LEAVE", "BAN", "UNBAN"
        """
        _ = self.bot.i18n.localize
        logchan = await self.bot.get_managed_channel(guild, "joins")
        if logchan is None:
            return

        guildperms = guild.me.guild_permissions
        chanperms = guild.me.permissions_in(logchan)
        if not (chanperms.send_messages and chanperms.view_channel):
            return

        emoji = get_constant_emoji(event, channel=logchan)
        new_join = ""
        reason = ""

        i18n_p = 'joinlog_'
        kwargs = {"channel": logchan, "guild": guild}

        if event == "JOIN":
            if member.created_at >= (datetime.datetime.now() - datetime.timedelta(days=1)):
                new_join = CONSTANTS['emojis']['NEW_JOIN']
        elif event in ["LEAVE", "BAN"]:
            action = discord.AuditLogAction.ban if event == "BAN" else discord.AuditLogAction.kick

            if guildperms.view_audit_log:
                await asyncio.sleep(CONSTANTS['audit_wait'])  # wait for audit logs to populate
                try:
                    async for entry in guild.audit_logs(action=action):
                        entry: discord.AuditLogEntry  # IDE helper
                        # i don't trust after=
                        if entry.created_at < (datetime.datetime.utcnow() - datetime.timedelta(seconds=15)):
                            break

                        if entry.target == member:
                            r = entry.reason if entry.reason else _('unavailable', **kwargs)
                            reason = _(i18n_p+'reason_mod', **kwargs).format(entry.user, r)

                            if event == "LEAVE":
                                event = "KICK"
                            break
                except discord.DiscordServerError:
                    pass

            # not using elif in case the previous check failed
            if event == "BAN" and (not reason) and guildperms.ban_members:
                ban = await guild.fetch_ban(member)
                reason = _(i18n_p+'reason', **kwargs).format(ban.reason)

        output = _(i18n_p + 'prefix', **kwargs) + _(i18n_p + event, **kwargs).strip()
        foutput = output.format(emoji, member, guild.member_count, new_join, reason, escape_markdown(str(member)))
        await self.joinlog_post(logchan, foutput, member, event)

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member):
        await self.joinlog_process(member, member.guild, "JOIN")

    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        await self.joinlog_process(member, member.guild, "LEAVE")

    @commands.Cog.listener()
    async def on_member_ban(self, guild: discord.Guild, member: discord.Member):
        await self.joinlog_process(member, guild, "BAN")

    @commands.Cog.listener()
    async def on_member_unban(self, guild: discord.Guild, member: discord.Member):
        await self.joinlog_process(member, guild, "UNBAN")

    @commands.Cog.listener()
    async def on_user_update(self, before: discord.User, after: discord.User):
        # todo: check if "if after in x.members" works
        name_change = before.name != after.name
        disc_change = before.discriminator != after.discriminator
        if not (name_change or disc_change):
            return
        guilds = (x for x in self.bot.guilds if discord.utils.get(x.members, id=after.id))
        for g in guilds:
            logchan = await self.bot.get_managed_channel(g, "name_change")
            if logchan is None:
                continue
            chanperms = g.me.permissions_in(logchan)
            if not (chanperms.send_messages and chanperms.view_channel):
                continue

            i18n_p = 'joinlog_'
            output = []
            if name_change:
                output.append(self.bot.i18n.localize(i18n_p+'NAME', channel=logchan, guild=logchan.guild))
            if disc_change:
                if not name_change:
                    i18n_p += 'ONLY_'
                output.append(self.bot.i18n.localize(i18n_p+'DISCRIM', channel=logchan, guild=logchan.guild))
            await self.joinlog_post(logchan, '\n'.join(output).format(before, after, escape_markdown(str(before)),
                                                                      escape_markdown(str(after))),
                                    after, 'USER_UPDATE')

    @commands.Cog.listener()
    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        _ = self.bot.i18n.localize

        if not after.type == discord.MessageType.default:
            return
        if after.content == before.content:
            return
        if not after.guild:
            return
        log_chan = await self.bot.get_managed_channel(after.guild, "messages")
        if log_chan is None:
            return
        perms = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages and perms.embed_links):
            return
        if after.author.id in self.bot.config_get(after.guild.id, "ignored_messages_log_users", [], False):
            return
        if not before.content:
            before.content = _('message_blank_content', channel=log_chan, guild=log_chan.guild)
        if not after.content:
            after.content = _('message_blank_content', channel=log_chan, guild=log_chan.guild)

        startkey = 'message_edit_'
        kwargs = {"channel": log_chan, "guild": log_chan.guild}
        def mlang(inpt): return _(startkey+inpt, **kwargs)

        embed = discord.Embed(title=shorten(mlang('title').format(after.channel), 256),
                              timestamp=after.created_at,
                              color=CONSTANTS['colors'][startkey[:-1]])
        embed.set_author(name=after.author.name, icon_url=after.author.avatar_url_as(format='png', size=128))
        embed.description = mlang('desc').format(after.jump_url)
        embed.add_field(name=mlang('old_title'), value=shorten(before.content, 1024), inline=False)
        embed.add_field(name=mlang('new_title'), value=shorten(after.content, 1024), inline=False)
        embed.set_footer(text=self.message_footer.format(after.author.id, after.id, after.channel.id))
        await log_chan.send(embed=embed)

    async def message_delete(self, message: typing.Union[discord.Message, Deletable, list], check_logs: bool):
        _ = self.bot.i18n.localize
        # get message data
        is_message = isinstance(message, discord.Message)
        if not is_message:
            channel = self.bot.get_channel(message.channel_id)
            if not channel.guild:
                return
            guild = channel.guild
            message_id = message.message_id
        else:
            if not message.guild:
                return
            guild = message.guild
            channel = message.channel
            message_id = message.id

        if not guild:  # ignore DMs or somethin
            return
        if is_message and message.author.id in self.bot.config_get(guild.id, "ignored_messages_log_users", [], False):
            return

        # get channel
        log_chan = await self.bot.get_managed_channel(guild, "messages")
        if log_chan is None:
            return
        # ensure perms
        perms: discord.Permissions = log_chan.permissions_for(log_chan.guild.me)
        if not (perms.read_messages and perms.send_messages and perms.embed_links):
            return

        # language variables
        startkey = 'message_delete_'
        kwargs = {"channel": log_chan, "guild": log_chan.guild}
        def mlang(inpt): return _(startkey + inpt, **kwargs)

        # embed formatting
        title_key = 'title' if is_message else 'uncached_title'
        title = mlang(title_key)
        embed = discord.Embed(title=shorten(title.format(channel), 256),
                              timestamp=discord.utils.snowflake_time(message_id),
                              color=CONSTANTS['colors'][startkey[:-1]])

        if is_message:
            embed.set_author(name=message.author.name, icon_url=message.author.avatar_url_as(format='png', size=128))
            embed.description = message.system_content  # no shortening necessary, desc limit is at least 2000

            # add likely deleter
            if perms.view_audit_log and check_logs:
                deleter = message.author
                await asyncio.sleep(CONSTANTS['audit_wait'])  # wait for audit logs to populate
                try:
                    async for entry in guild.audit_logs(limit=10, action=discord.AuditLogAction.message_delete):
                        entry: discord.AuditLogEntry  # IDE helper
                        # skip old entries because the after= parameter didn't really seem to work from my testing
                        if entry.created_at < (datetime.datetime.utcnow() - datetime.timedelta(minutes=15)):
                            break
                        # check if entry matches as much as possible
                        if entry.target == message.author and entry.extra.channel.id == channel.id:
                            deleter = entry.user
                            break
                    embed.add_field(name=mlang('deleter'),
                                    value=_('report_modmail_user_desc', **kwargs).format(deleter), inline=False)
                except discord.DiscordServerError:
                    pass

        author_id = message.author.id if is_message else _('unavailable', **kwargs)
        embed.set_footer(text=self.message_footer.format(author_id, message_id, channel.id))

        await log_chan.send(embed=embed)

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: discord.RawMessageDeleteEvent):
        if payload.cached_message:
            send = payload.cached_message
        else:
            send = Deletable(channel_id=payload.channel_id, message_id=payload.message_id)
        await self.message_delete(send, True)

    @commands.Cog.listener()
    async def on_raw_bulk_message_delete(self, payload: discord.RawBulkMessageDeleteEvent):
        for mid in payload.message_ids:
            cached_msg = discord.utils.get(payload.cached_messages, id=mid)
            if cached_msg is None:
                cached_msg = Deletable(channel_id=payload.channel_id, message_id=mid)
            await self.message_delete(cached_msg, False)

    async def log_roles(self, member: discord.Member, roles: typing.List[discord.Role], i18n_key: str):
        if not roles:
            return
        logchan = await self.bot.get_managed_channel(member.guild, "roles")
        if not logchan:
            return
        for role in roles:
            msg = self.bot.i18n.localize("joinlog_ROLES_"+i18n_key).format(member, role)
            await self.joinlog_post(logchan, msg, member, "ROLES")

    @commands.Cog.listener()
    async def on_member_update(self, before: discord.Member, after: discord.Member):
        await self.log_roles(after, [role for role in after.roles if role not in before.roles], "added")
        await self.log_roles(after, [role for role in before.roles if role not in after.roles], "removed")


def setup(bot):
    bot.add_cog(Joinlog(bot))
