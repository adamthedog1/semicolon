import asyncio

import discord
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.converters import Nullable, SnowflakeConvert, TEXT_CHANNEL
from cogs.utils.errors import SQLError
from cogs.utils.sql import TableManager
from cogs.utils.utils import add_attachment_info


class Starboard(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        bot.config_options['starboard_channel'] = Nullable(SnowflakeConvert(TEXT_CHANNEL, allow_partial=False))
        bot.config_options['starboard_threshold'] = int
        bot.config_options['starboard_emoji'] = str
        self.star_db = TableManager(self.bot.sql, 'starboard', [
            'starred_message_id integer PRIMARY KEY',
            'starboard_message_id integer NOT NULL'
        ])
        self.lock = asyncio.Lock()

    @staticmethod
    def star_emoji_for_count(count):
        # todo: use unicode codepoints as these emojis may not render in all IDEs
        return ['⭐', '🌟', '💫', '🌠', '✨'][min(4, count // 5)]

    @staticmethod
    def starboard_embed_accent_color(count):
        # todo: configurable color(s)
        lerp = min(count / 13, 1.0)

        # Linearly interpolate from off-white to yellow as the star count increases
        red = 255
        green = int((194 * lerp) + (253 * (1 - lerp)))
        blue = int((12 * lerp) + (247 * (1 - lerp)))
        return (red << 16) + (green << 8) + blue

    def make_starboard_message(self, message, count, star_emoji):
        emoji = self.star_emoji_for_count(count) if star_emoji == '⭐' else star_emoji

        content = f'{emoji} **{count}** {message.jump_url}'

        embed = discord.Embed(
            description=message.content,
            colour=self.starboard_embed_accent_color(count),
            timestamp=message.created_at
        )

        add_attachment_info(embed, message, self.bot.i18n)

        embed\
            .set_author(
                name=message.author.display_name,
                icon_url=message.author.avatar_url_as(format='png')
            ).set_footer(
                text=f'#{message.channel.name}'
            )

        return content, embed

    async def process_reaction(self, payload: discord.RawReactionActionEvent):
        if payload.guild_id is None:
            return
        star_emoji = self.bot.config_get(
            payload.guild_id,
            'starboard_emoji',
            default=self.bot.bot_config['starboard']['emoji'],
            return_none=False
        )
        if str(payload.emoji) != star_emoji:
            return

        channel = self.bot.get_channel(payload.channel_id)
        starboard_channel = await self.bot.get_managed_channel(channel.guild, 'starboard')
        if starboard_channel is None:
            return

        threshold = self.bot.config_get(
            payload.guild_id,
            'starboard_threshold',
            default=self.bot.bot_config['starboard']['threshold'],
            return_none=False
        )

        message = await channel.fetch_message(payload.message_id)

        count = 0
        for reaction in message.reactions:
            if str(reaction.emoji) == star_emoji:
                count = reaction.count
                # We only need to check if the author reacted.
                # In case there are a ton of reaction users (multiple pages' worth),
                #   this allows us to only fetch one, and because we're fetching after
                #   the snowflake just before the author ID, that one reaction user will
                #   always be the author if they exist. If not, it'll be another user, or nobody.
                async for user in reaction.users(after=discord.Object(message.author.id - 1), limit=1):
                    if user.id == message.author.id:
                        count -= 1
                break

        # TODO could have one lock per guild.
        #  should only create a lock upon first use, not at startup,
        #  to prevent creation of objects for servers that don't use starboard
        async with self.lock:
            try:
                # Message exists in starred-message database
                starboard_message_id = self.star_db.get_row_by({'starred_message_id': message.id}, cols = ['starboard_message_id'])[0]

                try:
                    starboard_message = await starboard_channel.fetch_message(starboard_message_id)
                    if count >= threshold:
                        content, embed = self.make_starboard_message(message, count, star_emoji)
                        await starboard_message.edit(content=content, embed=embed)
                    else:
                        await starboard_message.delete()
                except discord.errors.NotFound:
                    # Message was deleted from starboard; delete from database
                    # TODO either repost the message to starboard or add to "do not starboard" list
                    self.star_db.delete_row_by({'starred_message_id': message.id})
            except SQLError:
                if count >= threshold and message.channel.id != starboard_channel.id:
                    content, embed = self.make_starboard_message(message, count, star_emoji)
                    starboard_message = await starboard_channel.send(content=content, embed=embed)

                    self.star_db.create_row((message.id, starboard_message.id), ['starred_message_id', 'starboard_message_id'])

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        await self.process_reaction(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        await self.process_reaction(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_clear(self, payload: discord.RawReactionClearEvent):
        # Currently you can't clear reactions in DM threads, but discord.py says guild_id is Optional, so be safe here
        if payload.guild_id is None:
            return

        channel = self.bot.get_channel(payload.channel_id)
        starboard_channel = await self.bot.get_managed_channel(channel.guild, 'starboard')
        if starboard_channel is None:
            return

        async with self.lock:
            try:
                starboard_message_id = self.star_db.get_row_by({'starred_message_id': payload.message_id}, cols = ['starboard_message_id'])[0]

                try:
                    starboard_message = await starboard_channel.fetch_message(starboard_message_id)
                    await starboard_message.delete()
                finally:
                    self.star_db.delete_row_by({'starred_message_id': payload.message_id})
            except SQLError:
                pass

def setup(bot):
    bot.add_cog(Starboard(bot))
