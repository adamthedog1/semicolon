import asyncio
import inspect
import subprocess
import sys

import discord
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.errors import CogError
from cogs.utils.utils import get_constant_emoji, embed_author_template


def get_cog_path(cog: str):
    cogdir = 'cogs.'
    if not cog.startswith(cogdir):
        cog = cogdir + cog
    return cog


class Core(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot

    '''Core commands for bot developers'''
    # This cog is pretty much verbatim copied from
    # https://gitlab.com/HTSTEM/DiscordBot/-/blob/python-ext.com/cogs/core.py
    # which is licensed under MIT.

    @commands.command(aliases=['logout', 'exit', 'disconnect'])
    @commands.is_owner()
    async def quit(self, ctx: commands.Context):
        """Logs the bot out from Discord"""
        await ctx.send(self.bot.i18n.localize('logout', ctx=ctx))
        await ctx.bot.logout()

    @commands.command()
    @commands.is_owner()
    async def sudo(self, ctx: commands.Context, *, cmd: str):
        """Sudo make me a stale sandwich"""
        ctx.message.content = ctx.prefix + cmd
        await ctx.bot.process_commands_sudo(ctx.message)

    @commands.group(aliases=['error'], invoke_without_command=True)
    @commands.is_owner()
    async def crash(self, ctx: commands.Context):
        """Throws an exception"""
        raise Exception("Uh-oh oopsie-woopsie")

    @crash.command(aliases=['subcommand'])
    @commands.is_owner()
    async def sub(self, ctx: commands.Context, argument='arrrg'):
        """Throws an exception"""
        raise Exception(argument)

    @commands.command()
    @commands.is_owner()
    async def load(self, ctx: commands.Context, *, cog: str):
        """Loads an extension"""
        cog = get_cog_path(cog)

        try:
            ctx.bot.load_extension(cog)
        except Exception as e:
            raise CogError(cog, e, self.bot, ctx)
        else:
            await ctx.send('{} {}'.format(get_constant_emoji('SUCCESS', ctx), self.bot.i18n.localize('cog_load', ctx=ctx).format(cog)))

    @commands.command()
    @commands.is_owner()
    async def unload(self, ctx: commands.Context, *, cog: str):
        """Unloads an extension"""
        cog = get_cog_path(cog)

        try:
            ctx.bot.unload_extension(cog)
        except Exception as e:
            raise CogError(cog, e, self.bot, ctx)
        else:
            await ctx.send('{} {}'.format(get_constant_emoji('SUCCESS', ctx), self.bot.i18n.localize('cog_unload', ctx=ctx).format(cog)))

    @commands.group(invoke_without_command=True)
    @commands.is_owner()
    async def reload(self, ctx: commands.Context, *, cog: str):
        """Reloads an extension"""
        cog = get_cog_path(cog)

        try:
            ctx.bot.reload_extension(cog)
        except Exception as e:
            raise CogError(cog, e, self.bot, ctx)
        else:
            await ctx.send('{} {}'.format(get_constant_emoji('SUCCESS', ctx), self.bot.i18n.localize('cog_reload', ctx=ctx).format(cog)))

    @reload.command(name='all')
    @commands.is_owner()
    async def reload_all(self, ctx: commands.Context):
        """Reloads all extensions"""
        for extension in ctx.bot.extensions.copy():
            try:
                ctx.bot.reload_extension(extension)
            except Exception as e:
                raise CogError(extension, e, self.bot, ctx)

        await ctx.send('{} {}'.format(get_constant_emoji('SUCCESS', ctx), self.bot.i18n.localize('cog_reload_all', ctx=ctx).format(len(ctx.bot.extensions))))

    @reload.command(name='lang', aliases=['langs', 'i18n'])
    @commands.is_owner()
    async def reload_lang(self, ctx: commands.Context):
        """Reloads languages"""
        self.bot.i18n.load_langs()
        await ctx.send(self.bot.i18n.localize('cog_reload_lang', ctx=ctx))

    @commands.command(aliases=['git_pull', 'pull'])
    @commands.is_owner()
    @commands.bot_has_permissions(embed_links=True)
    async def update(self, ctx: commands.Context):
        """Updates the bot from git"""
        async with ctx.typing():
            if sys.platform == 'win32':
                process = subprocess.run('git pull', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = process.stdout, process.stderr
            else:
                process = await asyncio.create_subprocess_exec('git', 'pull', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = await process.communicate()
            stdout = stdout.decode().splitlines()
            stdout = '\n'.join('+ ' + i for i in stdout)
            stderr = stderr.decode().splitlines()
            stderr = '\n'.join('- ' + i for i in stderr)

            embed = embed_author_template(ctx, True)
            embed.add_field(name=self.bot.i18n.localize('git_pull', ctx=ctx), value="```diff\n{}\n{}```".format(stdout, stderr))
            await ctx.send(embed=embed)

    @commands.command(name='eval')
    @commands.is_owner()
    @commands.bot_has_permissions(embed_links=True)
    async def eval_cmd(self, ctx, *, code: str):
        """Evaluates code"""
        env = {
            'ctx': ctx,
            'bot': ctx.bot,
            'guild': ctx.guild,
            'author': ctx.author,
            'message': ctx.message,
            'channel': ctx.channel
        }
        env.update(globals())

        try:
            result = eval(code, env)

            if inspect.isawaitable(result):
                result = await result

            colour = 0x00FF00
        except Exception as e:
            result = type(e).__name__ + ': ' + str(e)
            colour = 0xFF0000

        embed = discord.Embed(colour=colour, title=code, description='```py\n{}```'.format(result))
        embed.set_author(name=ctx.author.display_name, icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Core(bot))
