import asyncio
import typing
from importlib import import_module

import discord
from discord.ext import commands
from discord.ext.commands.converter import _Greedy
from discord.utils import escape_markdown as esc_md

from cogs.utils.constants import CONSTANTS


def get_i18n_usage(ctx: commands.Context, command: typing.Union[commands.Command, commands.Group]):
    params = command.clean_params
    if not params:
        return ''

    result = []
    for name, param in params.items():
        # my code
        i18n_key = ","+command.qualified_name+"$"+name
        if i18n_key in ctx.bot.i18n.keys:
            name = ctx.bot.i18n.localize(i18n_key, ctx=ctx)
        # resume d.py code

        greedy = isinstance(param.annotation, _Greedy)

        if param.default is not param.empty:
            # We don't want None or '' to trigger the [name=value] case and instead it should
            # do [name] since [name=None] or [name=] are not exactly useful for the user.
            should_print = param.default if isinstance(param.default, str) else param.default is not None
            if should_print:
                result.append('[%s=%s]' % (name, param.default) if not greedy else
                              '[%s=%s]...' % (name, param.default))
            else:
                result.append('[%s]' % name)

        elif param.kind == param.VAR_POSITIONAL:
            if command.require_var_positional:
                result.append('<%s...>' % name)
            else:
                result.append('[%s...]' % name)
        elif greedy:
            result.append('[%s]...' % name)
        elif command._is_typing_optional(param.annotation):
            result.append('[%s]' % name)
        else:
            result.append('<%s>' % name)

    return ' '.join(result)


class SemiHelpCommand(commands.HelpCommand):
    def __init__(self):
        super().__init__(command_attrs={
            'cooldown': commands.Cooldown(1, 2.0, commands.BucketType.member)
        })
        self.add_check(commands.has_permissions(embed_links=True, add_reactions=True))

    def get_desc(self, command: commands.Command) -> str:
        ctx = self.context
        return ctx.bot.i18n.localize(command.name, ctx=ctx)

    def basic_embed(self) -> discord.Embed:
        ctx: commands.Context = self.context
        me: discord.User = ctx.bot.user
        embed = discord.Embed(title=ctx.bot.i18n.localize('help_title', ctx=ctx), color=CONSTANTS['colors']['default'])
        embed.set_footer(text=ctx.bot.i18n.localize('reason', ctx=ctx).format(str(ctx.author)))
        return embed.set_author(name=me.name, icon_url=me.avatar_url_as(size=128))

    async def send_bot_help(self, mapping: typing.Dict[typing.Optional[commands.Cog], typing.List[commands.Command]]):
        ctx: commands.Context = self.context
        mapping = {x: z for x, y in mapping.items() if (z := await self.filter_commands(y, sort=True))}

        pages: typing.List[discord.Embed] = []
        max_lines = CONSTANTS['lines_per_help_page']
        wrap = CONSTANTS['wrap_around_at']
        is_cont: typing.Dict[typing.Optional[commands.Cog], bool] = {x: False for x in mapping.keys()}
        while True:
            page_count = len(pages)+1
            embed = self.basic_embed()
            if page_count == 1:
                embed.title = ctx.bot.i18n.localize('help_main_title', ctx=ctx)

                args = (ctx.bot.user.name, CONSTANTS['source_code_branch']+'/README.md')
                embed.description = ctx.bot.i18n.localize('help_main_description', ctx=ctx).format(*args)

                l_args = (ctx.bot.invite_url, CONSTANTS['source_code'], CONSTANTS['source_code']+'/-/issues',
                          "https://wl.bs2k.ml/engage/semicolon/")
                embed.add_field(name=ctx.bot.i18n.localize('help_links_title', ctx=ctx),
                                value=ctx.bot.i18n.localize('help_links_description', ctx=ctx).format(*l_args),
                                inline=False)

                embed.add_field(name=ctx.bot.i18n.localize('help_nav_title', ctx=ctx),
                                value=ctx.bot.i18n.localize('help_nav_description', ctx=ctx).format(ctx.prefix,
                                                                                                    ctx.invoked_with)
                                +"\n\n\N{GLOBE WITH MERIDIANS} hello / hola / bonjour / السلام عليكم / привет /  안녕",
                                inline=False)
            else:
                embed.title = ctx.bot.i18n.localize('help_commands', ctx=ctx)
                line_count = 0
                for cog, _cmds in mapping.items():
                    # IDEs are still silly
                    cog: typing.Optional[commands.Cog]
                    _cmds: typing.List[commands.Command]

                    if not _cmds:
                        continue
                    if (max_lines - line_count) <= 0:
                        break
                    # this hurts my brain but i think it works
                    # (ensures a cog doesn't get cut off at 1 or 2 cmds)
                    line_count += 1
                    if max_lines-line_count < wrap <= len(_cmds):
                        break

                    cont = is_cont[cog]
                    is_cont[cog] = True
                    cmds = []

                    # get cog's displayed name
                    if cog is None:
                        name = ctx.bot.i18n.localize('help_unknown_cog', ctx=ctx)
                    else:
                        name = self.localize_cog_name(cog.qualified_name)

                    if cont:
                        name += ' ' + ctx.bot.i18n.localize('help_continued', ctx=ctx)
                    elif cog is not None:
                        key = ".!"+cog.qualified_name.lower()
                        if not (desc := ctx.bot.i18n.localize(key, ctx=ctx, default='')):
                            desc = cog.description
                        if desc:
                            cmds.append(desc.split('\n')[0])

                    cmds = []
                    while _cmds and max_lines-line_count > 0:
                        cmds.append(_cmds.pop(0))
                        line_count += 1

                    data = self.display_command_list(cmds)
                    embed.add_field(inline=False, name=name, value='\n'.join(data))

            pages.append(embed)
            if not any(mapping.values()):
                break

        pages = [em.set_footer(text=ctx.bot.i18n.localize('help_reason', ctx=ctx).format(c, len(pages),
                                                                                         str(ctx.author)))
                 for c, em in enumerate(pages, 1)]

        index = 0
        page = pages[index]
        can_remove = ctx.me.permissions_in(ctx.channel).manage_messages
        msg = await ctx.send(ctx.bot.i18n.localize('get_input_wait', ctx=ctx), embed=page)
        reacts = {
            "\N{BLACK LEFT-POINTING DOUBLE TRIANGLE}": 0,
            "\N{LEFTWARDS BLACK ARROW}": 1,
            "\N{BLACK RIGHTWARDS ARROW}": 2,
            "\N{BLACK RIGHT-POINTING DOUBLE TRIANGLE}": 3,
            "\N{GLOBE WITH MERIDIANS}": 4
        }
        for react in reacts.keys():
            await msg.add_reaction(react)

        def react_check(_react: discord.Reaction, _user: discord.User):
            return str(_react.emoji) in reacts.keys() and _user.id == ctx.author.id and _react.message.id == msg.id

        while True:
            try:
                await msg.edit(content='', embed=page)
                react, user = await ctx.bot.wait_for('reaction_add', timeout=60.0*3.0, check=react_check)
                if can_remove:
                    await msg.remove_reaction(react, user)

                # handle button
                action = reacts[str(react)]
                if action == 0:
                    index = 0
                elif action == 1:
                    index = max(index-1, 0)
                elif action == 2:
                    index = min(index+1, len(pages)-1)
                elif action == 3:
                    index = len(pages)-1
                elif action == 4:
                    await msg.delete()
                    prefs = import_module('cogs.prefs').Prefs(ctx.bot)
                    await prefs.lang(prefs, ctx)  # idk why i have to provide the "self" arg here lol
                    break

                page = pages[index]
            except (discord.Forbidden, asyncio.TimeoutError):
                for react in reacts.keys():
                    await msg.remove_reaction(react, ctx.me)
                break

    async def send_cog_help(self, cog: commands.Cog):
        ctx: commands.Context = self.context
        cmds: typing.List[commands.Command] = await self.filter_commands(cog.get_commands(), sort=True)

        embed = self.basic_embed()
        embed.title = self.localize_cog_name(cog.qualified_name)
        dkey = ".!" + cog.qualified_name.lower()
        if dkey in ctx.bot.i18n.keys:
            embed.description = ctx.bot.i18n.localize(dkey, ctx=ctx).format(pfx=ctx.prefix)
        elif cog.description:
            embed.description = cog.description.format(pfx=ctx.prefix)

        data = self.display_command_list(cmds, cog.get_commands())
        embed.add_field(name=ctx.bot.i18n.localize('help_commands', ctx=ctx), value='\n'.join(data))

        await ctx.send(embed=embed)

    async def send_group_help(self, group: commands.Group):
        ctx: commands.Context = self.context
        cmds: typing.List[commands.Command] = await self.filter_commands(group.commands, sort=True)

        embed = self.get_command_help(group)
        data = self.display_command_list(cmds, display_prefix=False)
        embed.add_field(name=ctx.bot.i18n.localize('help_subcommands', ctx=ctx), value='\n'.join(data))
        await ctx.send(embed=embed)

    def display_command_list(self, cmds: typing.List[typing.Union[commands.Command, commands.Group]],
                             all_cmds: typing.List[typing.Union[commands.Command, commands.Group]] = None,
                             display_prefix: bool = True)\
            -> typing.List[str]:
        if not cmds:
            if all_cmds is None or all_cmds:
                return [self.context.bot.i18n.localize('help_lacking', ctx=self.context)]
            else:
                return [self.context.bot.i18n.localize('help_no_commands', ctx=self.context)]
        prefix = self.context.prefix if display_prefix else ''
        cmd_names = {x: prefix + x.name
                        + ('*' if isinstance(x, commands.Group) else '') for x in cmds}
        fmt_len = max(map(len, cmd_names.values()))
        data = []
        for cmd, name in cmd_names.items():
            data.append(("`{{0:>{}}}` {{1}}".format(fmt_len).format(name, self.get_command_brief(cmd))).strip())
        return data

    async def send_command_help(self, command: commands.Command):
        await self.context.send(embed=self.get_command_help(command))

    def localize_cog_name(self, qual_name: str, prefix='>!'):
        ctx = self.context
        key = (prefix + qual_name).lower()
        val = ctx.bot.i18n.localize(key, ctx=ctx)
        if val != f"[{key}]":
            if not isinstance(val, str):
                val = val[0]
            return val
        return qual_name.split(' ')[-1]

    def get_command_help(self, command: typing.Union[commands.Command, commands.Group]) -> discord.Embed:
        ctx: commands.Context = self.context
        embed = self.basic_embed()
        embed.title = self.get_command_signature(command)

        output = [self.get_command_description(command).format(pfx=ctx.prefix)]
        cmd_name = command.qualified_name
        aliases = set(map(esc_md, filter(lambda x: x != cmd_name, [command.name] + command.aliases)))
        if aliases:
            output.insert(0, '')
            output.insert(0, ctx.bot.i18n.localize('help_aliases', ctx=ctx).format(', '.join(aliases)))
        embed.description = '\n'.join(output)
        return embed

    def get_command_brief(self, command: typing.Union[commands.Command, commands.Group]) -> str:
        ctx = self.context
        cmd_key = '<' + command.qualified_name
        val = ctx.bot.i18n.localize(cmd_key, ctx=ctx)
        if val != f"[{cmd_key}]":
            return val
        desc = self.get_command_description(command).splitlines()
        if not desc:
            return ''
        return desc[0].strip()

    def get_command_description(self, command: typing.Union[commands.Command, commands.Group]) -> str:
        ctx = self.context
        cmd_key = '.' + command.qualified_name
        val = ctx.bot.i18n.localize(cmd_key, ctx=ctx)
        if val != f"[{cmd_key}]":
            return val
        return command.help or ''

    def get_command_signature(self, command: typing.Union[commands.Command, commands.Group]) -> str:
        temp = command
        cmd_names = [command.qualified_name]
        while temp.parent:
            temp = temp.parent
            cmd_names.insert(0, temp.qualified_name)
        cmd_names_str = ' '.join(cmd_names)
        return ('%s%s %s' % (self.clean_prefix, cmd_names_str, get_i18n_usage(self.context, command))).strip()
