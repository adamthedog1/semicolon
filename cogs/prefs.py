from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.utils import get_input


class Prefs(commands.Cog, name='Bot Preferences'):
    def __init__(self, bot: Semicolon):
        self.bot = bot

    async def get_lang_input(self, ctx: commands.Context, prompt: str):
        prompt += '\n' + self.bot.i18n.localize('lang_select_desc_3', ctx=ctx)\
            .format("https://wl.bs2k.ml/engage/semicolon/")
        langs = [['_', self.bot.i18n.localize("lang_select_default", ctx=ctx)]]
        langs += [[x, y['_NAME_']] for x, y in self.bot.i18n.langs.items()]
        # todo: could maybe make this a sorted dict instead?? is that a thing? idk
        langlist = sorted(langs, key=lambda x: x[1].lower())

        langs = [x[1] for x in langlist]
        langkeys = [x[0] for x in langlist]
        return await get_input(self.bot.i18n.localize("lang_select_title", ctx=ctx), prompt, langs, langkeys, ctx=ctx)

    def get_current(self, ctx: commands.Context, snowflake: str):
        # try/except not really needed but just being safe :)
        try:
            if snowflake is not None:
                current_name = self.bot.i18n.langs[snowflake]['_NAME_']
                return '\n' + self.bot.i18n.localize("lang_select_desc_2", ctx=ctx).format(current_name)
        except KeyError:
            self.bot.i18n.clear_removed_langs()
        return ''

    @commands.group(aliases=['language', 'i18n'], invoke_without_command=True)
    @commands.bot_has_permissions(embed_links=True)
    async def lang(self, ctx):
        """
        Set your language for the bot. Relies on community translations.
        """
        prompt = self.bot.i18n.localize("lang_select_desc_user", ctx=ctx)
        prompt += self.get_current(ctx, self.bot.i18n.get_config(str(ctx.author.id)))
        langkey = await self.get_lang_input(ctx, prompt)
        if langkey is None:
            return
        self.bot.i18n.write_config(langkey, str(ctx.author.id))

    @lang.command(name='channel')
    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.guild_only()
    async def lang_channel(self, ctx):
        """
        Sets the language for the current channel.
        """
        prompt = self.bot.i18n.localize("lang_select_desc_gc", ctx=ctx).format(ctx.channel.mention)
        prompt += self.get_current(ctx, self.bot.i18n.get_config(str(ctx.channel.id), is_channel=True))
        langkey = await self.get_lang_input(ctx, prompt)
        if langkey is None:
            return
        self.bot.i18n.write_config(langkey, str(ctx.channel.id), is_channel=True)

    @lang.command(name='guild')
    @commands.has_permissions(manage_guild=True)
    @commands.bot_has_permissions(embed_links=True)
    @commands.guild_only()
    async def lang_guild(self, ctx):
        """
        Sets the language for the current guild.
        """
        prompt = self.bot.i18n.localize("lang_select_desc_gc", ctx=ctx).format(ctx.guild.name)
        prompt += self.get_current(ctx, self.bot.i18n.get_config(str(ctx.guild.id), is_guild=True))
        langkey = await self.get_lang_input(ctx, prompt)
        if langkey is None:
            return
        self.bot.i18n.write_config(langkey, str(ctx.guild.id), is_guild=True)


def setup(bot):
    bot.add_cog(Prefs(bot))
