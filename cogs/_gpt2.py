import asyncio
import datetime
import logging
import os
import random
import re
import typing

import discord
from discord.ext import tasks, commands
from numpy import math

from cogs.utils.bot import Semicolon
from cogs.utils.errors import ExpectedError, UserInputError
from cogs.utils.utils import guild_check, data_path, clamp, line_split, ping_user

LOCK = asyncio.Lock()
newline_split = re.compile(r'\n?<\|endoftext\|>\n')


async def gen_sample(conditional_input: str = '', complete: bool = None):
    """Hacky function to generate samples from the gpt-2 instance"""
    async with LOCK:
        path = open('gpt2.txt', 'r').read().split('\n')[0].strip()
        # path = os.path.join(os.path.expanduser('~'), 'GitHub', 'gpt-2')
        cmd = "bash htstem_gen.sh"
        if conditional_input:
            cmd = "bash htstem_condition.sh"
            inputpath = os.path.join(path, 'input.txt')
            if not complete:
                conditional_input += '\n<|endoftext|>\n'
            with open(inputpath, 'w') as f:
                f.write(conditional_input)
        process = await asyncio.create_subprocess_shell(cmd,
                                                        stdout=asyncio.subprocess.PIPE,
                                                        stderr=asyncio.subprocess.PIPE,
                                                        cwd=path)
        stdout, stderr = await process.communicate()
        if process.returncode != 0:
            logging.getLogger("bot").exception("Samples did not properly generate!")
            return
        samples = stdout.decode().split("=" * 40 + " SAMPLE 1 " + "=" * 40+'\n')[1]
        return newline_split.split(samples)


async def is_stem(ctx: commands.Context) -> bool:
    """Returns bool of if current guild is STEM or not"""
    return guild_check(ctx, 282219466589208576)


class CommandProcessing(ExpectedError):
    def __init__(self):
        super().__init__('Please wait for existing commands to finish.')


class SampleFailure(ExpectedError):
    def __init__(self):
        super().__init__('Sample output failed, please try again.')


class GPT2(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.random_prompt.start()

    @tasks.loop(hours=5)
    async def random_prompt(self):
        dtnow = datetime.datetime.utcnow().replace(microsecond=0, second=0, minute=0)
        post_timer_hours = 6
        wait_hour = post_timer_hours * math.ceil(dtnow.hour / post_timer_hours)
        wait_until = dtnow + datetime.timedelta(hours=wait_hour-dtnow.hour)
        # TODO: sometimes this gives results that immediately post, should probably look into, adding this hack for now
        if wait_until < datetime.datetime.utcnow():
            return
        self.bot.logger.debug(f'Sample sleeping until {wait_until}')
        await discord.utils.sleep_until(wait_until)
        self.bot.logger.debug('Sending sample')
        samples = await gen_sample()
        botespam = self.bot.get_channel(282500390891683841)
        async with botespam.typing():
            await botespam.send(samples[0])

    @random_prompt.before_loop
    async def before_random_prompt(self):
        await self.bot.wait_until_ready()

    def cog_unload(self):
        self.random_prompt.cancel()

    @commands.command()
    @commands.is_owner()
    async def get_training_data(self, ctx: commands.Context):
        """Gets training data for a GPT-2 model :)"""
        async with ctx.typing():
            datapath = data_path('{}.txt'.format(ctx.guild.id))
            botself = ctx.guild.me
            sadreplace = re.compile(r'no(ah|elle)kiq', re.IGNORECASE)
            for chan in ctx.guild.text_channels:
                if chan.category_id == 360588732333817857:  # avoid htstem mod channels
                    continue
                perms = chan.permissions_for(botself)
                if not (perms.read_messages and perms.read_message_history):
                    continue
                async for msg in chan.history(limit=None, oldest_first=True):
                    if random.random() > 0.2:
                        continue
                    msgcontent = msg.clean_content  # or use .system_content if you want <@id>'s and @e's
                    if not msgcontent:  # if msg content is blank
                        continue
                    with open(datapath, 'a') as f:
                        f.write('{}\n<|endoftext|>\n'.format(sadreplace.sub("lexikiq", msgcontent)))
            await ctx.reply('done lol')

    @commands.group(aliases=['gpt2'])
    @commands.check(is_stem)
    async def prompt(self, ctx: commands.Context):
        """Communicate with a GPT-2 117M instance trained on HTwins STEM.
        Specify "enable" as the first argument to have the AI finish your message instead of responding to it.
        GPT-2 117M is trained on various text around the internet and may provide random results.
        It was tuned overnight using a fifth of the messages in HTwins STEM (~95k) public channels.
        (Only a fifth was used to avoid out of memory errors. I need more RAM...)
        It went through 76k training steps and reached a loss of ~0.08, where it was stopped to avoid overfitting."""
        if ctx.invoked_subcommand is None:
            await ctx.send_help(ctx.command)

    @prompt.command(usage='[optional: messages to generate] <input sentence(s)>')
    @commands.check(is_stem)
    async def respond(self, ctx: commands.Context, messages: typing.Optional[int] = 1, *, input_text: str):
        """Responds to your message"""
        if LOCK.locked():
            raise CommandProcessing()
        async with ctx.typing():
            samples = await gen_sample(input_text)
            messages = clamp(messages, 1, min(5, len(samples)))
            if not samples:
                raise SampleFailure()
            if messages == 1:
                for msg in line_split('{}: {}'.format(ctx.author.mention, samples[0])):
                    await ctx.send(msg, allowed_mentions=ping_user(ctx))
                return
        for msg_c in range(0, messages):
            for msg in line_split('**[{}]** {}'.format(msg_c+1, samples[msg_c])):
                await ctx.send(msg)

    @prompt.command(usage='<input sentence(s)>')
    @commands.check(is_stem)
    async def complete(self, ctx: commands.Context, *, input_text: str):
        """'Auto-completes' your input text. May not complete anything."""
        if LOCK.locked():
            raise CommandProcessing()
        async with ctx.typing():
            samples = await gen_sample(input_text, True)
            if not samples:
                raise SampleFailure()
            for msg in line_split('{}: {}{}'.format(ctx.author.mention, input_text, samples[0])):
                await ctx.send(msg, allowed_mentions=ping_user(ctx))

    @prompt.command()
    @commands.check(is_stem)
    async def channel(self, ctx: commands.Context, channel: discord.TextChannel, messages: typing.Optional[int] = 1):
        """Responds to the last 10 messages of a specified channel.
        [messages] refers to the number of messages that should be generated in response."""
        if LOCK.locked():
            raise CommandProcessing()
        async with ctx.typing():
            g_perms = ctx.guild.default_role.permissions.read_messages
            c_perms = channel.overwrites_for(ctx.guild.default_role).read_messages
            if channel.category_id == 360588732333817857 or not (g_perms and (c_perms or c_perms is None)):
                raise UserInputError(ctx.bot.i18n.localize('error', ctx=ctx)+'Nice try, pal.')
            botself = ctx.guild.me
            perms = channel.permissions_for(botself)
            if not (perms.read_messages and perms.read_message_history):
                raise UserInputError(ctx.bot.i18n.localize('error', ctx=ctx) +
                                     "I don't have permission to read that channel!")
            msgs = []
            async for msg in channel.history(limit=10, oldest_first=True):
                msgs.append(msg.clean_content)
            samples = await gen_sample('\n<|endoftext|>\n'.join(msgs))
            if not samples:
                raise SampleFailure()
            messages = clamp(messages, 1, min(5, len(samples)))
            if messages == 1:
                for msg in line_split('{}: {}'.format(ctx.author.mention, samples[0])):
                    await ctx.send(msg, allowed_mentions=ping_user(ctx))
                return
        for msg_c in range(0, messages):
            for msg in line_split('**[{}]** {}'.format(msg_c + 1, samples[msg_c])):
                await ctx.send(msg)


def setup(bot):
    bot.add_cog(GPT2(bot))
